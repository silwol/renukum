extern crate clap;
extern crate find_folder;
extern crate piston_window;

use clap::{Arg,App};
use piston_window::*;
use std::path::Path;

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
		.arg(Arg::with_name("datapath")
			 .help("Path to folder containing the game data")
			 .required(true)
			 .index(1))
        .get_matches();
    let datapath = matches.value_of("datapath").unwrap_or(".");
    let graphics_path = Path::new(datapath).join("graphics");

    let opengl = OpenGL::V3_2;
    let mut window: PistonWindow =
        WindowSettings::new(env!("CARGO_PKG_NAME"), [300, 300])
        .exit_on_esc(true)
        .opengl(opengl)
        .build()
        .unwrap();

    let item = graphics_path.join("actors").join("energydrink.png");
    println!("{:?}", item);
    let item = Texture::from_path(
        &mut window.factory,
        &item,
        Flip::None,
        &TextureSettings::new()
        ).unwrap();
    while let Some(e) = window.next() {
        window.draw_2d(&e, |c, g| {
            clear([1.0; 4], g);
            image(&item, c.transform, g);
        });
    }
}
