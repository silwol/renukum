extern crate glob;

use glob::glob;
use std::env;
use std::fs::DirBuilder;
use std::path::Path;
use std::process::Command;
use std::result::Result;

fn main() {
    let out_dir = &env::var("OUT_DIR").unwrap();
    let subdir = &"assets/graphics";

    for from_path in glob(&format!("{}/**/*.svg", subdir)).unwrap().filter_map(Result::ok) {
        let to_path = Path::new(out_dir).join(from_path.with_extension("png"));
        DirBuilder::new()
            .recursive(true)
            .create(to_path.parent().unwrap()).unwrap();

        Command::new("inkscape")
            .arg(&"--export-png")
            .arg(to_path)
            .arg(from_path)
            .status().unwrap();
    }
}
